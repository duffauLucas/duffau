﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Hotel
    {
        private int id;
        private string nom;
        private string adresse;
        private string ville;

        //constructeur
        public Hotel(int unId, string unNom, string uneAdresse, string uneVille)
        {
            this.Id = unId;
            this.Nom = unNom;
            this.Adresse = uneAdresse;
            this.Ville = uneVille;
        }

        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string Ville { get => ville; set => ville = value; }
    }
}
